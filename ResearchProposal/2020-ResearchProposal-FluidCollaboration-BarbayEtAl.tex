% Created 2020-10-18 Sun 10:59
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Jérémy Barbay}
\date{\today}
\title{\small Research Proposal: \\
\LARGE \emph{Fluid Collaboration}}
\hypersetup{
 pdfauthor={Jérémy Barbay},
 pdftitle={\small Research Proposal: \\
\LARGE \emph{Fluid Collaboration}},
 pdfkeywords={},
 pdfsubject={Research Proposal on Fluid Collaboration},
 pdfcreator={Emacs 26.2 (Org mode 9.1.9)}, 
 pdflang={English}}
\begin{document}

\maketitle
\begin{abstract}
We propose a \texttt{decentralized collaborative framework}, which we call ``\emph{Fluid Collaboration}'', where users participate in both the production of new material, and in the evaluation of the quality of such material by merely searching, using or modifying material in a collaborative database. 

As proof of concept, we propose to develop two to four applications of such a framework to promote the collaboration of educators over time (reusability of past material from the same institution or author) and space (usability of material from other institutions or authors) for the production of digital material for transmitting and evaluating knowledge: \texttt{Jitsi Flip} for online flipped classroom, \texttt{Easy Exam} for the production of sequence of problems for theoretical assignments and exams, \texttt{Easy Program Checking} for the production of programming assignments with automated test checking and semi-automated correction, and \texttt{Easy Quiz} for the production of sequences of multi choice questions.  Such application target populations in various scopes, from international English speaking academics for \texttt{Jitsi Flip} (e.g. adopters of the online flipped classroom methodology among the 3000 instructed by Eric Mazur in April and May 2020), \texttt{Easy Exam} and \texttt{Easy Program Checking} (e.g. international community of academic computer science instructors), to local Spanish speaking high-school profesors for \texttt{EasyQuiz} (e.g. profesors preparing their students for the standardized national exam punctuating the transition between secundary and academic education).  For each application, the database of material will be boot strapped with existing material (e.g.  examples of \emph{concept questions} from Eric Mazur for \texttt{Jitsi Flip}, problems from past exams and programming assignments for \texttt{Easy Exam} and \texttt{Easy Program Checking}, and \emph{quizzes} from past PSU tests in the case of \texttt{Easy Quiz}), as such for examples of desirable material, and manually altered for examples of undesired material, based on a selection of six objective quality criteria, elected through public surveys of the target population of each application.

Qualitative and quantitative evaluation will allow to validate not only whether such applications are usable and producing elements of reasonable to good quality, but also whether they also promote collaborations over both time and space. The qualitative evaluation will consist in requesting users to fill surveys before and after the evaluation period of the application. The quantitative application will log and export the graph of reusability of each component of the database in a properly desagregated format, so that a selection of quality and reusability statistics can be extracted.

Encouraging and helping people to collaborate has always been crucial, and is all the more important in the context of international and national stresses, caused whether by medical, economic o political turmoil.

\emph{Keywords:} 
\end{abstract}

\section{Introduction}
\label{sec:org00d15e1}

The human species is a collaborative one: not only individuals isolated for their peers are hardly able to survive, but humanity as a species has achieved many feats (e.g. sending probes to other planets) which would not have been achievable by any single individual, or even by small groups of individuals.  Even though naturalists and game theorists have given many examples of collaborative systems which are \textbf{not} hierarchical, most human collaborative systems are inherently hierarchical, with the top part of the hierarchy organizing the collaboration (e.g. planification, quality control, reward and punishment) and its bottom part actually implementing it (e.g. producing food). Such hierarchies have permitted great achievements, and their cost (in resources, in speed of execution and in reaction time to new conditions) has sometimes been presented as a necessary one to such achievements.

Some recent advances in Human Computer Interactions (e.g. Crowd-Sourcing, Re-Captcha) have allowed flater types of collaborations (e.g. Mechanical Turks, wikipedia), where the quality control is semi-automatized (positive and negative examples mixed within the task flow of workers) on one hand; and fluider collaborations (e.g. the Amazon clients buying items A,B,C,D,E are unaware that they are also collectively producing a list of suggestions for Amazon clients buying items A,B,C,D) on the other hand. Existing applications of such advances are still based on a hierarchy (e.g. Wikipedia editors) and external rewards (e.g. Mechanical Turks workers work for money), yet those do not appear to be strict requirements: could digital collaborators be part of a non-hierarchical structure and rewarded with the (parts of the) result of the collaboration itself, forming a positive feedback loop for the system to grow in size and quality, as a \texttt{Boot Strapping Database}?

In order to answer such a question, we propose the concept of ``\emph{Fluid Collaboration}'', a framework for \texttt{collaboration with decentralized quality control}, where users participate in both the production of new material and in the quality control of such material by merely searching, using or modifying material in a collaborative database. Such a framework is obviously not completely new, and builds upon previously existing ones: we both describe a selection of such applications and formalize the concept of Fluid Collaboration in Section \hyperref[sec:orgca4f417]{Collaborative Frameworks and Applications}.

As proof of concept, we propose to develop two to four applications of such a framework, each aiming to promote the collaboration of educators over time (reusability of past material from the same institution or author) and space (usability of material from other institutions or authors) for the production of digital material for transmitting and evaluating knowledge, described in details in Section \hyperref[sec:org35614e0]{Proofs of Concept}: \texttt{Jitsi Flip} for online flipped classroom, \texttt{Easy Quiz} for the production of sequences of multi choice questions, and eventually  \texttt{Easy Exam} for the production of sequence of problems for theoretical assignments and exams, and \texttt{Easy Program Checking} for the production of programming assignments with automated test checking and semi-automated correction. Such applications target populations in various scopes, from international English speaking academics for \texttt{Jitsi Flip} (e.g. adopters of the online flipped classroom methodology among the 3000 instructed by Eric Mazur in April and May 2020), \texttt{Easy Exam} and \texttt{Easy Program Checking} (e.g. international community of academic computer science instructors), to local Spanish speaking high-school profesors for \texttt{EasyQuiz} (e.g. profesors preparing their students for the standardized national exam punctuating the transition between secundary and academic education). For each application, the database of material will be boot strapped with existing material (e.g.  examples of \emph{concept questions} from Eric Mazur for \texttt{Jitsi Flip}, problems from past exams and programming assignments for \texttt{Easy Exam} and \texttt{Easy Program Checking}, and \emph{quizzes} from past PSU tests in the case of \texttt{Easy Quiz}), as such for examples of desirable material, and manually altered for examples of undesired material, based on a selection of six objective quality criteria, elected through public surveys of the target population of each application. And each application will be evaluated both qualitatively and quantitatively, in order to validate not only whether such applications are usable and producing elements of reasonable to good quality, but also whether they also promote collaborations over both time and space: the qualitative evaluation will consist in requesting users to fill surveys before and after the evaluation period of the application, and the quantitative application will log and export the graph of reusability of each component of the database in a properly desagregated format, so that a selection of quality and reusability statistics can be extracted.

Collaboration has always been crucial, both to the survival of member of the human race and to the achivement of its most amazing feats, and is all the more important in the context of international and national stresses, whether caused by medical, economic o political turmoil: we discuss the relevance of this project in general and in such times in the Section \hyperref[sec:orgac88f5e]{Relevance}.
\section{Background:}
\label{sec:orga7b6008}
\subsection{Collaborative Frameworks and Applications}
\label{sec:orgca4f417}
\subsubsection{Existing Frameworks}
\label{sec:org8150af5}
\subsubsection{Existing Applications}
\label{sec:org71911ea}
\begin{enumerate}
\item Wikipedia
\label{sec:orgd9229bc}
\item Re-Captcha
\label{sec:org8abeedc}
\item Amazon Turks
\label{sec:orgc210083}
\end{enumerate}
\section{Project}
\label{sec:org1256dfb}
\subsection{Definitions}
\label{sec:org7e01387}
\begin{description}
\item[{Fluid Collaboration}] People collaborate with people \textbf{without conscious overcost} (for coordination or trust). This occurs naturally between people who regularly collaborate, and controled environments can be configurated so that it occurs between people who don't know each other at small scale.
\item[{Boot Strapping Database}] Crowd Sourcing where the reward is auto-generated by the crowd (as opposed to external influx of resources in crowd-sourcing such as Mechanical Turks), and where collective quality control mechanisms (e.g. Re Captcha) insure the equity of the collaboration bettween participants (e.g. no or few free loaders).
\end{description}
\subsection{Hypothesis}
\label{sec:org4a38185}
\begin{enumerate}
\item Some educational material and educative evaluation material (e.g. quizzes and homeworks) present components (e.g. questions and problems) which are reusable enough across time and space to justify collaborating via the reusal of such components over those ranges.
\item Being provided by clients providing a fluid collaboration interface (and other advantages), instructors will contribute at once new components (e.g. new questions and problems) and quality information about the components they use (e.g. adequacy and difficulty), which in turn will permit better suggestions of components to instructors.
\item A finite initial set of components, along with their quality assesment, will be enough to motivate instructors to interact with the system and add to it more components and quality assesments.
\end{enumerate}
\subsection{Research Questions}
\label{sec:org5997cab}
\begin{enumerate}
\item Which components of educational material and educative evaluation material can be reused and to which extent (e.g. questions about Turbo Pascal or Fortran might be now inadequate)?
\item How much additional advantage (e.g. a web interface for students to fill quizzes at any time and receive immediate feedback) is required in order to motivate instructors 1) to use a cloud based interface to generate their material, 2) to input additional material in the system and 3) to validate existing material in the system.
\item How large must an initial set of components be to boot-strap the interest of such a database of components among instructors?
\end{enumerate}
\subsection{Objectives}
\label{sec:orgb6475bb}
\begin{enumerate}
\item DEFINE a \texttt{protocol} for
\begin{itemize}
\item \textbf{fluid collaboration clients} to connect to
\item \textbf{boot strapping databases} so that
\item to promote \textbf{equitable collaboration}
\begin{itemize}
\item between \textbf{large number of agents} (well beyond sizes where everybody can know each other),
\item \textbf{across time} (reuse of old items) and
\item \textbf{across space} (reuse of items created by others),
\item in a way which improves over time both the \textbf{volume} and the \textbf{quality} of the content of the database
\end{itemize}
\end{itemize}

\item IMPLEMENT \texttt{first examples} of
\begin{itemize}
\item an \textbf{open data standard} for questionaries and quizes,
\item \textbf{two fluid collaboration clients}:
\begin{itemize}
\item a simple one helping profesors to generate quizes with several multichoice questions [Ricardo Cordova]
\item a more complex one integrating such quizes to Online Flipeed Classroom [Sven Reisenegger],
\end{itemize}
\item \textbf{a boot strapping database}: [Ricardo Cordova]
\begin{itemize}
\item managing the data and meta data acquired from the various clients to guide the fluid collaboration between the various users across time and space.
\end{itemize}
\end{itemize}

\item EVALUATE the benefits of using such tools for instructos and students.

\item DESCRIBE other examples of applications of the pair Fluid Collaboration/Boot Strapping Database (data standards and fluid collaboration clients), such as
\begin{enumerate}
\item \texttt{written homework assignement} composed of various written exercizes or problems (coming with full solutions, marking scheme, an statistics on how difficult it was for students);
\item \texttt{programming homework assignment} coming with scripts to give automatical feedback to students, a plateform to give an online ranking of the projects submitted by students, a script to cluster the homework assignments in order to speed-up their evaluation by humans;
\item \texttt{video exams} and \texttt{video capsules} coming with a plateform to guide the post-calibrated peer review of pedagogical videos submitted by students, which can later be used as video capsules in an online flipped classroom.
\end{enumerate}

\item DEFINE a \texttt{protocol} for boot strapping databases to connect between themselves, in order to insure further scalability of such solutions.
\end{enumerate}

\section{Proofs of Concept}
\label{sec:org35614e0}
\subsection{Easy Quiz}
\label{sec:orgb633291}
\subsubsection{Application}
\label{sec:org14c1fd9}
\subsubsection{Targeted Public}
\label{sec:org6af98a5}
\subsubsection{Initial Data Set}
\label{sec:orgce2f277}
\subsubsection{Validation process}
\label{sec:org5644c14}
\begin{enumerate}
\item Qualitative
\label{sec:org425f56a}
\item Quantitative
\label{sec:org984315d}
\end{enumerate}
\subsubsection{Analysis}
\label{sec:orgb97658f}
\subsection{Jitsi Flip}
\label{sec:orgba7e3c6}
\subsubsection{Application}
\label{sec:org9bc618e}
\subsubsection{Targeted Public}
\label{sec:orge63e86c}
\subsubsection{Initial Data Set}
\label{sec:org63f3d35}
\subsubsection{Validation process}
\label{sec:org276ea04}
\begin{enumerate}
\item Qualitative
\label{sec:org567e9a8}
\item Quantitative
\label{sec:org46987d4}
\end{enumerate}
\subsubsection{Analysis}
\label{sec:orgb06c238}
\subsection{Easy Exam}
\label{sec:org9c0ab18}
\subsubsection{Application}
\label{sec:org044e767}
\subsubsection{Targeted Public}
\label{sec:orgbc4470f}
\subsubsection{Initial Data Set}
\label{sec:orgbba8449}
\subsubsection{Validation process}
\label{sec:org3407b4b}
\begin{enumerate}
\item Qualitative
\label{sec:orgf9a4579}
\item Quantitative
\label{sec:org99a6084}
\end{enumerate}
\subsubsection{Analysis}
\label{sec:orgd2e314f}
\subsection{Easy Program Exam}
\label{sec:org760a3dc}
\subsubsection{Application}
\label{sec:org9b8e852}
\subsubsection{Targeted Public}
\label{sec:org6da76fb}
\subsubsection{Initial Data Set}
\label{sec:orgae4c211}
\subsubsection{Validation process}
\label{sec:orgcc76605}
\begin{enumerate}
\item Qualitative
\label{sec:org84e1019}
\item Quantitative
\label{sec:orgaa92faf}
\end{enumerate}
\subsubsection{Analysis}
\label{sec:org43a80af}
\section{Relevance}
\label{sec:orgac88f5e}
Encouraging and helping people to collaborate has always been crucial, from the survival to the biggest successes of humanity: humans are born collaborators. Most human collaborative systems are hierarchical, with inherent costs in resources and reaction time [HANDY Model], such that the largest collaborations require the most organisation. We hope that pushing forward the concept of Fluid Collaboration, by reducing the need for a centralized controlling hierarchy and hence its inherent cost, will generally promote more collaboration; allow to solve more efficiently old problems (the production of educational material on which we focus in this proposal being only one example among others); and potentially allow to solve problems previously not considered solvable (e.g. world-wide collaborative strategies to curb population and consumption growth, to maintain or nurture back bio diversity, and/or to deal with climate and pandemic emergencies).

It might seem harder to promote collaboration in times of international and national stresses, caused whether by medical, economic o political turmoil: the human narrative (e.g. books, movies) describes at lenght individual and selfish reactions in such situations (e.g. stocking, ransacking, etc.). But recent studies [``L'entre-aide, l'autre loi de la Jungle'' by Pablo Servigne, Gauthier Chapelle, 2017] showed that, to the contrary, collaboration is not only more needed in times of crisis, it is also naturally more frequent in such times, in general both between individuals from the same or from distinct species [Tree study], and in particular between humans [Metro Camera study]. As educators across the world confront various crisis of their own (e.g. move to online teaching, teaching students in dire situations, etc.), it is our hope that the proof of concept prototypes describes in this proposal will find a positive reception not only beside difficult situations, but because of them. As many difficult situations will arise in the incoming years, it is our hope that these proof of concepts will inspire other similar collaborative efforts.
Emacs 26.2 (Org mode 9.1.9)
\end{document}
