#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline author:t c:nil creator:comment d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t timestamp:t toc:nil todo:t |:t
#+TITLE: Fluid Collaboration
#+AUTHOR: Jérémy Barbay
#+EMAIL: jeremy@barbay.cl
#+DESCRIPTION: A decentralized collaborative framework applied to the collaboration of educators across time and space.
#+KEYWORDS:
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.4.1 (Org mode 8.2.5h)
#+OPTIONS: texht:t
#+DATE: \today
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS:
#+LATEX_HEADER:
#+LATEX_HEADER_EXTRA: \usepackage{fullpage}

#+begin_abstract  
#+end_abstract 
  
* Descriptions
** Elevator pitch:

The project =Fluid Collaboration= proposes to facilitate collaboration on the creation and redaction of digital document in collaboration across time and space, in particular for education.

** For Users

Instead of each person working on his own, =Fluid Collaboration= tools guide the creation by decomposing documents in atomic components, and facilitate their reuse across time and space by suggesting existing fragments found to be relevant.

** 4 Ideas principales para apoyar a los docentes
     1) la decomposicion del material docente en componentes atomicos "reusables" (e.g. texto de un problema en una tarea) y "poco reusables" (e.g. el titulo, la fecha, los nombres de los profesores, de la seccion y de la institucion en una tarea);
     2) la indexacion y sistemas de suggerencia para los docentes (e.g. dado la materia que ya usaste, la siguiente te podria servir, a la "amazon", "youtube", etc., pero maximizando el apoyo a los docentes en vez del ingreso monetario!);
     3) el desarrollo de applicacion "clientes" entregando mas apoyo aun (e.g. scripts de feedback automatizado para Tareas de Programacion, interfaz de peer review entre alumnos) al costo de mas esfuerzo de desarrollo del profesor (i.e. entregar varios casos de pruebas para cada tarea de programacion), pero que se puede amotizar via el reuso facilitado por los puntos 1 y 2.
     4) la captura (cuando se puede) del rendimiento de los alumnos sobre el material docente (inspirado de GAFAM) para entregar (via estadisticas) a los docentes informacion *experimental* sobre la calidad del material que consideran reusar.

** Romper con la "artesanía" donde cada instructor en cada institucion "reinventa la rueda"

El proyecto =Fluid Collaboration in Education= propone de

  1. romper con la "artesanía" donde cada instructor en cada institucion "reinventa la rueda",
     1) creando su proprio curriculum y curso y
     2) material expositorio y
     3) material de evaluacion (muchas veces muy similar a lo que se hacer en otras partes); via la
     
  2. creación de un ecosystema decentralizado que ayuda tales profesores a
     1) compartir el material que crean (que sea de curricular, de docencia o de evaluacion),
     2) compartir el rendimiento de tal material en practica (i.e. agregacion de estadisticas sobre la recepcion por los alumnos, incluyendo sus notas)
     3) buscar a dentro del material compartido por otros (por relevancia *y* por calidad, evaluada por pares y experimentalmente) y
     4) reusarlo facilamente (haciendo el material modular, e.g. separando la fecha del examen de los problemas que contiene); facilitado por la
     
  3. creación de herramientas (costosas pero cual costo de desarrollo esta amortizado sobre cantidad masivas de usuarios) para facilitar las tareas docentes) que sirven de "zanahorias" para los primeros usuarios de cada comunidad (cuando no hay mucho material a reusar aun), tal que
     1) Servidor de Evaluacion por pares de videos pedagogicos cortos -> Proyecto =Easy Video Clases= de mi alumno de Magister Jorge Ampuero;
     2) Interfazes amigables de produccion -> Proyecto =Easy Quiz= de mi alumno de Magister Ricardo Cordova;
     3) Interfaz de video-conferencia *optimizada* por la docencia (e.g. Zoom fue disegnado por reuniones, no para la docencia! Jitsi es open source y se puede modificar) -> Proyecto =Easy Flip= de mi alumno de Memoria Sven Reisenegger, desarrollando Breakout Rooms para Jitsi, con varias features specificas a la docencia en linea;
     4) Servidor de Evaluacion semi-automatizada -> Proyecto =Easy Program Checking= de (varios alumnos pero ahora de) mi alumno Blaz Korecic;
     5) Servidor de Evaluacion por pares (Pre Calibrated y Post Calibrated) -> Proyecto Easy Peer Review de mi alumno de magister en TI Patricio Moya.

* Public
* Milestones and Sub-Projects
* Resources required

